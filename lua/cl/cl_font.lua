surface.CreateFont("RashkinskRodchenko2", {
	font = "RodchenkoCTT",
	extended = true,
	size = (ScrW() + ScrH()) * .03,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko3", {
	font = "RodchenkoCTT",
	extended = true,
	size = (ScrW() + ScrH()) * .021,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko8", {
	font = "RodchenkoCTT",
	extended = true,
	size = 23,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko10", {
	font = "Futura-Normal",
	extended = true,
	size = 20,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko7", {
	font = "RodchenkoCTT",
	extended = true,
	size = (ScrW() + ScrH()) * .0004,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko4", {
	font = "RodchenkoCTT",
	extended = true,
	size = (ScrW() + ScrH()) * .01,
	weight = 700,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenkoVote", {
	font = "RodchenkoCTT",
	extended = true,
	size = (ScrW() + ScrH()) * .0065,
	weight = 700,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko9", {
	font = "RodchenkoCTT",
	extended = true,
	size = 21.34,
	weight = 700,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko5", {
	font = "RodchenkoCTT",
	extended = true,
	size = 35,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskRodchenko6", {
	font = "RodchenkoCTT",
	extended = true,
	size = (ScrW() + ScrH()) * .015,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskFutura2", {
	font = "Futura-Normal",
	extended = true,
	size = (ScrW() + ScrH()) * .01,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskFutura3", {
	font = "Futura-Normal",
	extended = true,
	size = (ScrW() + ScrH()) * .0065,
	weight = 100,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskFutura4", {
	font = "Futura-Normal",
	extended = true,
	size = (ScrW() + ScrH()) * .009,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont("RashkinskFutura9", {
	font = "Futura-Normal",
	extended = true,
	size = 19,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true
})

surface.CreateFont( "RashkinskRodchenko1", {
	font = "RodchenkoCTT",
	extended = true,
	size = 21,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )

surface.CreateFont( "RashkinskFutura1", {
	font = "Futura-Normal",
	extended = true,
	size = 20,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )

surface.CreateFont( "RashkinskFuturaSmall", {
	font = "Futura-Normal",
	extended = true,
	size = (ScrW() + ScrH()) * .0065,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )