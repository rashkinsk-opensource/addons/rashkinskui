local mainf4 = {ptbl ={}}

local screenwidth = ScrW()
local screenheight = ScrH()

local scrw1 = 1/1350*screenwidth
local scrh1 = 1/768*screenheight

local function canGetJob(job)
    local ply = LocalPlayer()

    if isnumber(job.NeedToChangeFrom) and ply:Team() ~= job.NeedToChangeFrom then return false, false end
    if istable(job.NeedToChangeFrom) and not table.HasValue(job.NeedToChangeFrom, ply:Team()) then return false, false end
    if job.customCheck and not job.customCheck(ply) then return false, false end
    --if ply:Team() == job.team then return false, true end
    if job.max ~= 0 and ((job.max % 1 == 0 and team.NumPlayers(job.team) >= job.max) or (job.max % 1 ~= 0 and (team.NumPlayers(job.team) + 1) / #player.GetAll() > job.max)) then return false, false end
    if job.admin == 1 and not ply:IsAdmin() then return false, true end
    if job.admin > 1 and not ply:IsSuperAdmin() then return false, true end


    return true
end

local function canBuyEntity(item)
    local ply = LocalPlayer()

    if istable(item.allowed) and not table.HasValue(item.allowed, ply:Team()) then return false, true end
    if item.customCheck and not item.customCheck(ply) then return false, true end

    local canbuy, suppress, message, price = hook.Call("canBuyCustomEntity", nil, ply, item)
    local cost = price or item.getPrice and item.getPrice(ply, item.price) or item.price
    if not ply:canAfford(cost) then return false, false, message, cost end

    if canbuy == false then
        return false, suppress, message, cost
    end

    return true, nil, message, cost
end

local function canBuyShipment(ship)
    local ply = LocalPlayer()

    if not table.HasValue(ship.allowed, ply:Team()) then return false, true end
    if ship.customCheck and not ship.customCheck(ply) then return false, true end

    local canbuy, suppress, message, price = hook.Call("canBuyShipment", nil, ply, ship)
    local cost = price or ship.getPrice and ship.getPrice(ply, ship.price) or ship.price

    if not ply:canAfford(cost) then return false, false, message, cost end

    if canbuy == false then
        return false, suppress, message, cost
    end

    return true, nil, message, cost
end

local function canBuyGun(ship)
    local ply = LocalPlayer()

    if GAMEMODE.Config.restrictbuypistol and not table.HasValue(ship.allowed, ply:Team()) then return false, true end
    if ship.customCheck and not ship.customCheck(ply) then return false, true end

    local canbuy, suppress, message, price = hook.Call("canBuyPistol", nil, ply, ship)
    local cost = price or ship.getPrice and ship.getPrice(ply, ship.pricesep) or ship.pricesep

    if not ply:canAfford(cost) then return false, false, message, cost end

    if canbuy == false then
        return false, suppress, message, cost
    end

    return true, nil, message, cost
end

local function canBuyAmmo(item)
    local ply = LocalPlayer()

    if item.customCheck and not item.customCheck(ply) then return false, true end

    local canbuy, suppress, message, price = hook.Call("canBuyAmmo", nil, ply, item)
    local cost = price or item.getPrice and item.getPrice(ply, item.price) or item.price
    if not ply:canAfford(cost) then return false, false, message, cost end

    if canbuy == false then
        return false, suppress, message, cost
    end
    return true, nil, message, cost
end

local function canBuyVehicle(item)
    local ply = LocalPlayer()
    local cost = item.getPrice and item.getPrice(ply, item.price) or item.price

    if istable(item.allowed) and not table.HasValue(item.allowed, ply:Team()) then return false, true end
    if item.customCheck and not item.customCheck(ply) then return false, true end

    local canbuy, suppress, message, price = hook.Call("canBuyVehicle", nil, ply, item)

    cost = price or cost

    if not ply:canAfford(cost) then return false, false, message, cost end

    if canbuy == false then
        return false, suppress, message, cost
    end

    return true, nil, message, cost
end


local function canBuyFood(food)
    local ply = LocalPlayer()

    if (food.requiresCook == nil or food.requiresCook == true) and not ply:isCook() then return false, true end
    if food.customCheck and not food.customCheck(LocalPlayer()) then return false, false, nil, food.price end

    if not ply:canAfford(food.price) then return false, false, nil, food.price end

    return true, false, nil, food.price
end

function Derma_StringRequest( strTitle, strText, strDefaultText, fnEnter, fnCancel, strButtonText, strButtonCancelText )

	local Window = vgui.Create( "DFrame" )
	Window:SetTitle( "" )
	Window:SetDraggable( false )
	Window:ShowCloseButton( false )
	Window:SetBackgroundBlur( true )
	Window:SetDrawOnTop( true )
	Window.Paint = function (self,w,h)
			surface.SetDrawColor(Rashkinsk.UIColors.Grey)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(Rashkinsk.UIColors.Red)
			--surface.SetMaterial(Rashkinsk.UIMaterials.grad)
			surface.DrawRect(0, 0, w, screenheight*0.0455729167)
			--surface.DrawTexturedRect(0, 0, w, 30)
			--surface.DrawTexturedRect(0, 0, w, 30)
			--surface.DrawTexturedRect(0, 0, w, 30)

			draw.SimpleText(strText,"RashkinskRodchenko8",screenwidth*0.0073529412,screenheight*0.0065104167,color_white)
	end

	local InnerPanel = vgui.Create( "DPanel", Window )
	InnerPanel:SetPaintBackground( false )

	local Text = vgui.Create( "DLabel", InnerPanel )
	Text:SetText( "" )
	Text:SetFont("RashkinskRodchenko8")
	Text:SizeToContents()
	Text:SetContentAlignment( 5 )
	Text:SetTextColor( color_white )

	local TextEntry = vgui.Create( "DTextEntry", InnerPanel )
	TextEntry:SetText( strDefaultText or "" )
	TextEntry.Paint = function (self,w,h)
		surface.SetDrawColor(color_white)
		surface.SetMaterial(Rashkinsk.UIMaterials.grad)
		surface.DrawRect(0, 0, w, h)
		surface.DrawTexturedRect(0, 0, w, h)
		surface.DrawTexturedRect(0, 0, w, h)
		surface.DrawTexturedRect(0, 0, w, h)
		draw.SimpleText( TextEntry:GetValue() ,"RashkinskRodchenko10",w/2,h/2,color_black,1,1)
	end
	TextEntry.OnEnter = function() Window:Close() fnEnter( TextEntry:GetValue() ) end

	local ButtonPanel = vgui.Create( "DPanel", Window )
	ButtonPanel:SetTall( screenheight*0.0286458333 )
	ButtonPanel:SetPaintBackground( false )
--[[
	local Button = vgui.Create( "DButton", ButtonPanel )
	Button:SetText( strButtonText or "OK" )
	Button:SizeToContents()
	Button:SetTall( 20 )
	Button:SetWide( Button:GetWide() + 20 )
	Button:SetPos( 5, 5 )
	Button.DoClick = function() Window:Close() fnEnter( TextEntry:GetValue() ) end
]]

	local Buttoncheck = vgui.Create( "DImageButton", ButtonPanel )
	Buttoncheck:Dock(LEFT)				// Set position
	Buttoncheck:SetSize( 24*scrw1, 24*scrh1 )			// OPTIONAL: Use instead of SizeToContents() if you know/want to fix the size
	Buttoncheck:SetImage( Rashkinsk.UIMaterials.check )	// Set the material - relative to /materials/ directory
	--Buttoncheck:SizeToContents()				// OPTIONAL: Use instead of SetSize if you want to resize automatically ( without stretching )
	Buttoncheck.DoClick = function() Window:Close() fnEnter( TextEntry:GetValue() ) end


	local ButtonCancel = vgui.Create( "DImageButton", ButtonPanel )
	ButtonCancel:Dock(RIGHT)				// Set position
	ButtonCancel:SetSize( 22*scrw1, 22*scrh1 )			// OPTIONAL: Use instead of SizeToContents() if you know/want to fix the size
	ButtonCancel:SetImage( Rashkinsk.UIMaterials.cross )	// Set the material - relative to /materials/ directory
	--Buttoncheck:SizeToContents()				// OPTIONAL: Use instead of SetSize if you want to resize automatically ( without stretching )
	ButtonCancel.DoClick = function() Window:Close() if ( fnCancel ) then fnCancel( TextEntry:GetValue() ) end end
--[[
	local ButtonCancel = vgui.Create( "DButton", ButtonPanel )
	ButtonCancel:SetText( strButtonCancelText or "Cancel" )
	ButtonCancel:SizeToContents()
	ButtonCancel:SetTall( 20 )
	ButtonCancel:SetWide( Buttoncheck:GetWide() + 20 )
	ButtonCancel:SetPos( 5, 5 )
	ButtonCancel.DoClick = function() Window:Close() if ( fnCancel ) then fnCancel( TextEntry:GetValue() ) end end
	ButtonCancel:MoveRightOf( Buttoncheck, 5 )
]]
	ButtonPanel:SetWide( Buttoncheck:GetWide() + 5*scrw1 + ButtonCancel:GetWide() + 10*scrw1 )

	surface.SetFont("RashkinskRodchenko8")
	local w, h = surface.GetTextSize(strText)
	w = math.max( w )

	Window:SetSize( w + screenwidth*0.0147058824, h + screenheight*0.1432291667 )
	Window:Center()

	InnerPanel:StretchToParent( 5, 25, 5, 45 )

	Text:StretchToParent( 5, 5, 5, 35 )

	TextEntry:StretchToParent( 5, nil, 5, nil )
	TextEntry:AlignBottom( 5 )

	TextEntry:RequestFocus()
	TextEntry:SelectAllText( true )

	ButtonPanel:CenterHorizontal()
	ButtonPanel:AlignBottom( 8 )

	Window:MakePopup()
	Window:DoModal()

	return Window

end

timersimpleasd = 0
hook.Add("Think","CloseF4Menu",function()
	if CurTime() > timersimpleasd then
		if input.IsKeyDown(KEY_F4) then
			if IsValid(mainf4.ptbl.base) then
				mainf4.ptbl.base:MoveTo( -5000,ScrH()/2-(mainf4.ptbl.base:GetTall()/2), 0.5, 0, -1)
				timer.Simple(0.2,function() mainf4.ptbl.base:Close() end)
			end
			timersimpleasd = CurTime() + 1
		end
	end
end)

net.Receive("rushkf4menu",function()

	if IsValid(mainf4.ptbl.base) then mainf4.ptbl.base:Close() end

    local drawRashkinskBG = GWEN.CreateTextureBorder(0, 0, 4100, 2025, 0, 0, 0, 0, Rashkinsk.UIMaterials.bgcherno)
            
	local ply = LocalPlayer()
    local scrw, scrh = ScrW(), ScrH()
    local totalw, totalh = ScrH()*1.2,  ScrH()*.80
    
    mainf4.ptbl.base = vgui.Create("DFrame")
    mainf4.ptbl.base:SetSize(totalw,totalh)
    mainf4.ptbl.base:MakePopup()
    mainf4.ptbl.base:Center()
    mainf4.ptbl.base:SetTitle("")
    mainf4.ptbl.base:ShowCloseButton(false)
    mainf4.ptbl.base.Paint = function (self,w,h)
        drawRashkinskBG(0,0,w,h)
    end

	mainf4.ptbl.base.Tabs = {}
	
	local tabSize = 55
	
	mainf4.ptbl.base.TabTitle = vgui.Create("DLabel", mainf4.ptbl.base)
	mainf4.ptbl.base.TabTitle:SetFont("RashkinskRodchenko2")
	mainf4.ptbl.base.TabTitle:SizeToContents()
	mainf4.ptbl.base.TabTitle:SetTextColor(color_white)
	
	local function AddTab(name, mat, createFunc)
		local tab = vgui.Create("DButton", mainf4.ptbl.base)
		tab:SetSize(tabSize*scrw1, tabSize*scrw1)
		tab:SetPos(scrw1*15 + (tabSize*scrw1 + scrw1*10) * table.Count(mainf4.ptbl.base.Tabs), scrh1*25)
		tab:SetText("")
		
		function tab:Paint(w, h)
			if mainf4.ptbl.base.CurrentTab == name or self.Depressed or self.Hovered then
				draw.RoundedBox(4, 0, 0, w, h, color_white)
			else
				Rashkinsk.DrawMasked(function() Rashkinsk.RoundedBoxEx(4, 0, 0, w, h, color_white, true, true, true, true) end, 
				function() 
					surface.SetDrawColor(Rashkinsk.UIColors.Red)
					surface.SetMaterial(Rashkinsk.UIMaterials.grad)
					surface.DrawRect(0, 0, w, h)
					surface.DrawTexturedRectRotated(w/2, h/2, w, h, 90)
					surface.DrawTexturedRectRotated(w/2, h/2, w, h, 90)
					surface.DrawTexturedRectRotated(w/2, h/2, w, h, 90)
				end)
			end
			surface.SetDrawColor(color_white)
			surface.SetMaterial(mat)
			surface.DrawTexturedRect(scrw1*5, scrh1*5, w - scrw1*10, h - scrh1*10)
		end
		function tab:DoClick()
			if mainf4.ptbl.base.CurrentTab != name then
				if IsValid(mainf4.ptbl.base.CurrentTabPanel) then
					mainf4.ptbl.base.CurrentTabPanel:Remove()
				end
				mainf4.ptbl.base.CurrentTab = name
				mainf4.ptbl.base.CurrentTabPanel = createFunc(mainf4.ptbl.base)
				mainf4.ptbl.base.TabTitle:SetText(name)
			end
		end
		
		if !mainf4.ptbl.base.CurrentTab then
			tab:DoClick()
		end
		
		mainf4.ptbl.base.Tabs[name] = tab
		mainf4.ptbl.base.TabTitle:SetPos(scrw1*15 + (tabSize*scrw1 + scrw1*10) * table.Count(mainf4.ptbl.base.Tabs), 23*scrh1)
		mainf4.ptbl.base.TabTitle:SetSize(scrw1*1000, tabSize*scrh1)
	end
		
	local commands = {
		[1] = {
			name = "Дать денег",
			DoClick = function()
				Derma_StringRequest("Кол-во денег", "Сколько вы хотите дать денег?", "0", function(a)
					RunConsoleCommand("DarkRP", "give", tostring(a))
					mainf4.ptbl.base:Close()
				end)
			end,
		},
		[2] = {
			name = "Выбросить деньги",
			DoClick = function()
				Derma_StringRequest("Кол-во денег", "Сколько денег вы хотите бросить?", "0", function(a)
					RunConsoleCommand("DarkRP", "dropmoney", tostring(a))
					mainf4.ptbl.base:Close()
				end)
			end,
		},
		[3] = {
			name = "Выписать чек",
			DoClick = function()
				local menu = DermaMenu()
				for _,ply in pairs(player.GetAll()) do
					if ply ~= LocalPlayer() then
						menu:AddOption(ply:Nick(), function()
							Derma_StringRequest("Кол-во денег", "На какую сумму выписать чек игроку ?", "0",
								function(a)
								RunConsoleCommand("darkrp", "cheque", ply:UserID(), a)
								mainf4.ptbl.base:Close()
								end, function() end )
						end)
					end
				end
				menu:Open()
			end,
		},
		[4] = {
			name = "Выкинуть текущее оружие",
			DoClick = function()
				RunConsoleCommand("darkrp", "drop")
				mainf4.ptbl.base:Close()
			end,
		},
		[5] = {
			name = "Продать всю недвижимость",
			DoClick = function()
				RunConsoleCommand("darkrp", "unownalldoors")
				mainf4.ptbl.base:Close()
			end,
		},
		/*[6] = {
			IsLabel = true,
			name = "Сменить работу",
			DoClick = function(but, text)
				RunConsoleCommand("darkrp", "job", but:GetValue())
			end,
		},*/
		[6] = {
			name = "Уволить другого игрока",
			DoClick = function()
				local menu = DermaMenu()
				for _,ply in pairs(player.GetAll()) do
					if ply ~= LocalPlayer() then
						menu:AddOption(ply:Nick(), function()
							Derma_StringRequest("Причина увольнения", "Почему вы хотите уволить игрока "..ply:Nick().."?", nil,
								function(a)
								RunConsoleCommand("darkrp", "demote", ply:UserID(), a)
								end, function() end )
						end)
					end
				end
				menu:Open()
			end,
		},
		[7] = {
			name = "Вызвать службы",
			DoClick = function()
				ServiceMenu(LocalPlayer():GetPos())
				mainf4.ptbl.base:Close()
			end,
		},
	}
	
	local professionCommands = {}
	
	if ply:isCP() then
		table.insert(professionCommands, {
			name = "Ордер на обыск",
			DoClick = function()
				local menu = DermaMenu()
				for _,ply in pairs(player.GetAll()) do
					if not ply:getDarkRPVar("warrant") and ply ~= LocalPlayer() then
						menu:AddOption(ply:Nick(), function()
							Derma_StringRequest("Обыск", "Почему вы хотите обыскать игрока "..ply:Nick().."?", nil,
								function(a)
								//LocalPlayer():ConCommand("say /warrant ".. tostring(ply:UserID()).." ".. a)
								RunConsoleCommand("darkrp", "warrant", ply:UserID(), a)
								end, function() end ) end)
					end
				end
				menu:Open()
			end,
		})
		table.insert(professionCommands, {
			name = "Объявить в розыск",
			DoClick = function()
				local menu = DermaMenu()
				for _,ply in pairs(player.GetAll()) do
					if not ply:getDarkRPVar("wanted") and ply ~= LocalPlayer() then
						menu:AddOption(ply:Nick(), function()
							Derma_StringRequest("Розыск", "Почему вы хотите объявить игрока "..ply:Nick().." в розыск?", nil,
								function(a)
								//LocalPlayer():ConCommand("say /warrant ".. tostring(ply:UserID()).." ".. a)
								RunConsoleCommand("darkrp", "wanted", ply:UserID(), a)
								end, function() end ) end)
					end
				end
				menu:Open()
			end,
		})
		
		table.insert(professionCommands, {
			name = "Снять розыск",
			DoClick = function()
				local menu = DermaMenu()
				for _,ply in pairs(player.GetAll()) do
					if ply:getDarkRPVar("wanted") and ply ~= LocalPlayer() then
						menu:AddOption(ply:Nick(), function() RunConsoleCommand("darkrp", "unwanted", ply:UserID()) end)
					end
				end
				menu:Open()
			end,
		})
		local ismayor, ischief = false, false
		for k,v in pairs(player.GetAll()) do
			if v:isChief() then
				ischief = true
			elseif v:isMayor() then
				ismayor = true//"Установить позицию тюрьмы (сброс)"
			end
		end
		if ply:isChief() or ply:IsAdmin() then
			table.insert(professionCommands, {
				name = "Установить тюрьму (сброс)",
				DoClick = function()
					RunConsoleCommand("DarkRP", "jailpos")
				end,
			})
			table.insert(professionCommands, {
				name = "Добавить тюрьму",
				DoClick = function()
					RunConsoleCommand("DarkRP", "addjailpos")
				end,
			})
		end
		if (ply:isChief() and !ismayor) or ply:isMayor() then
			table.insert(professionCommands, {
				name = "Дать лицензию",
				DoClick = function()
					RunConsoleCommand("DarkRP", "givelicense")
				end,
			})
		end
	end
	
	if ply:isMayor() then
		table.insert(professionCommands, {
			name = "Установить ком. час",
			DoClick = function()
				RunConsoleCommand("DarkRP", "lockdown")
			end,
		})
		table.insert(professionCommands, {
			name = "Отменить ком. час",
			DoClick = function()
				RunConsoleCommand("DarkRP", "unlockdown")
			end,
		})
		table.insert(professionCommands, {
			name = "Разместить свод законов",
			DoClick = function()
				RunConsoleCommand("DarkRP", "placelaws")
			end,
		})
		table.insert(professionCommands, {
			name = "Добавить закон",
			DoClick = function()
				Derma_StringRequest("Добавить закон", "Введите формулировку закона, который вы бы хотели добавить.", "", function(law)
					RunConsoleCommand("DarkRP", "addlaw", law)
				end)
			end,
		})
		table.insert(professionCommands, {
			name = "Отменить закон",
			DoClick = function()
				Derma_StringRequest("Отменить закон", "Введите номер закона, который вы хотите отменить.", "", function(num)
					RunConsoleCommand("DarkRP", "removelaw", num)
				end)
			end,
		})
	end

	AddTab("Команды", Rashkinsk.UIMaterials.commands, function(frame)
		local mainPanel = vgui.Create("DPanel", frame)
		mainPanel:SetPos(-10*scrw1, 10*scrh1)
		mainPanel:SetSize(totalw, totalh)
		
		function mainPanel:Paint(w, h)
		end
		
		mainPanel:MoveToBack()
		
		mainPanel.ModelPanel = vgui.Create("DModelPanel", mainPanel)
		mainPanel.ModelPanel:SetSize(totalw * .3, totalh)
		mainPanel.ModelPanel:SetPos(totalw * .70, 0)
		
		mainPanel.ModelPanel:SetLookAt(Vector(-100,0,-22))
		mainPanel.ModelPanel:SetFOV(36)
		
		function mainPanel.ModelPanel:SetModel(model,seq)
			if IsValid(self.Entity) then
				self.Entity:Remove()
				self.Entity = nil
			end

			if !ClientsideModel then return end
			self.Entity = ClientsideModel(Model(model), RENDER_GROUP_OPAQUE_ENTITY)
			if !IsValid(self.Entity) then return end
			self.Entity:SetNoDraw(true)
			local iseq = 0
			if seq then
				iseq = self.Entity:LookupSequence(seq) 
			end
			if iseq > 0 then
				self.Entity:ResetSequence(iseq)
			end
			self.Entity:SetPos( Vector( -55, 0, -50 ) )
		end
		mainPanel.ModelPanel:SetCamPos(Vector(0,0,0))
		mainPanel.ModelPanel:SetDirectionalLight(BOX_LEFT,Color(200, 90, 0, 50))
		mainPanel.ModelPanel:SetAmbientLight(Color(0, 0, 0))
		mainPanel.ModelPanel.Angles = Angle(0, mainf4.ptbl.base.ModelRotation, 0)
		function mainPanel.ModelPanel:DragMousePress()
			self.PressX, self.PressY = gui.MousePos()
			self.Pressed = true
		end

		function mainPanel.ModelPanel:DragMouseRelease() self.Pressed = false end
		function mainPanel.ModelPanel:LayoutEntity( Entity )
			if ( self.bAnimated ) then self:RunAnimation() end

			if ( self.Pressed ) then
				local mx, my = gui.MousePos()
				self.Angles = self.Angles - Angle( 0, ( self.PressX or mx ) - mx, 0 )
				
				frame.ModelRotation = self.Angles.y

				self.PressX, self.PressY = gui.MousePos()
			end

			Entity:SetAngles( self.Angles )
		end
		
		mainPanel.ModelPanel:SetModel(ply:GetModel(), "idle_all_02")
		
		function mainPanel.ModelPanel.Entity:GetPlayerColor()
			return ply:GetPlayerColor()
		end
		
		mainPanel.ButtonsList = vgui.Create("DPanelList", mainPanel)
		mainPanel.ButtonsList:SetSize(totalw/3 * 2 - 20*scrw1, totalh - (25*scrh1 + tabSize*scrh1 + 20*scrh1))
		mainPanel.ButtonsList:SetPos(10*scrw1, 25*scrh1 + tabSize*scrh1 + 10*scrh1)
		function mainPanel.ButtonsList:Paint(w, h)
		end
		mainPanel.ButtonsList:SetSpacing(5*scrh1)
		mainPanel.ButtonsList:EnableVerticalScrollbar(true)
		mainPanel.ButtonsList.VBar:SetHideButtons(true)
		function mainPanel.ButtonsList.VBar:Paint(w, h)
		end
		
		function mainPanel.ButtonsList.VBar.btnGrip:Paint(w, h)
			surface.SetDrawColor(color_white)
			surface.DrawRect(2, 0, w - 2, h)
		end
		
		for k,v in pairs(commands) do
			if v.IsLabel then
				local label = vgui.Create("DLabel", mainPanel.ButtonsList)
				label:SetText(v.name)
				label:SetTall(60*scrh1)
				label:SetFont("RashkinskRodchenko3")
				label:SetTextColor(color_white)
				mainPanel.ButtonsList:AddItem(label)
				local textEntry = vgui.Create("DTextEntry", mainPanel.ButtonsList)
				textEntry.OnEnter = v.DoClick
				textEntry.OnLoseFocus = textEntry.OnEnter
				textEntry:SetFont("RashkinskRodchenko3")
				textEntry:SetTall(40*scrh1)
				mainPanel.ButtonsList:AddItem(textEntry)
			else
				local button = vgui.Create("DButton", mainPanel.ButtonsList)
				button:SetTall(70*scrh1)
				button.DoClick = v.DoClick
				button:SetFont("RashkinskRodchenko3")// 25 * 2 потому что юникод
				button:SetText(v.name)
				button:SetTextColor(color_black)
				function button:Paint(w, h)
					surface.SetDrawColor(color_white)
					surface.DrawRect(0, 0, w, h)
					if !self.Hovered and !self.Depressed then
						surface.SetMaterial(Rashkinsk.UIMaterials.grad)
						surface.DrawTexturedRect(0, 0, w, h)
						surface.DrawTexturedRect(0, 0, w, h)
					end
				end
				mainPanel.ButtonsList:AddItem(button)
			end
		end
		
		if table.Count(professionCommands) > 0 then
			mainPanel.ButtonsList:AddItem(profLabel)
			
			for k,v in pairs(professionCommands) do
				local button = vgui.Create("DButton", mainPanel.ButtonsList)
				button:SetTall(70*scrh1)
				button.DoClick = v.DoClick
				button:SetFont("RashkinskRodchenko3")
				button:SetText(v.name)
				button:SetTextColor(color_black)
				function button:Paint(w, h)
					surface.SetDrawColor(color_white)
					surface.DrawRect(0, 0, w, h)
					if !self.Hovered and !self.Depressed then
						surface.SetMaterial(Rashkinsk.UIMaterials.grad)
						surface.DrawTexturedRect(0, 0, w, h)
						surface.DrawTexturedRect(0, 0, w, h)
					end
				end
				mainPanel.ButtonsList:AddItem(button)
			end
		end
		
		
		return mainPanel
	end)
	AddTab("Профессии", Rashkinsk.UIMaterials.jobs, function(frame)
		local selectedJob
		
		local mainPanel = vgui.Create("DPanel", frame)
		mainPanel:SetPos(0, 0)
		mainPanel:SetSize(totalw, totalh)
		
		function mainPanel:Paint(w, h)
		end
		
		mainPanel:MoveToBack()
		
		mainPanel.ModelPanel = vgui.Create("DModelPanel", mainPanel)
		mainPanel.ModelPanel:SetSize(totalw/4, totalh)
		mainPanel.ModelPanel:SetPos(totalw/2.8 * 2, 0)
		
		mainPanel.ModelPanel:SetLookAt(Vector(-100,0,-22))
		mainPanel.ModelPanel:SetFOV(36)
		
		function mainPanel.ModelPanel:SetModel(model,seq)
			if IsValid(self.Entity) then
				self.Entity:Remove()
				self.Entity = nil
			end

			if !ClientsideModel then return end
			self.Entity = ClientsideModel(Model(model), RENDER_GROUP_OPAQUE_ENTITY)
			if !IsValid(self.Entity) then return end
			self.Entity:SetNoDraw(true)
			local iseq = 0
			if seq then
				iseq = self.Entity:LookupSequence(seq) 
			end
			if iseq > 0 then
				self.Entity:ResetSequence(iseq)
			end
			self.Entity:SetPos( Vector( -55, 0, -50 ) )
		end
		mainPanel.ModelPanel:SetCamPos(Vector(0,0,0))
		mainPanel.ModelPanel:SetDirectionalLight(BOX_LEFT,Color(200, 90, 0, 50))
		mainPanel.ModelPanel:SetAmbientLight(Color(0, 0, 0))
		mainPanel.ModelPanel.Angles = Angle(0, mainf4.ptbl.base.ModelRotation, 0)
		function mainPanel.ModelPanel:DragMousePress()
			self.PressX, self.PressY = gui.MousePos()
			self.Pressed = true
		end

		function mainPanel.ModelPanel:DragMouseRelease() self.Pressed = false end
		function mainPanel.ModelPanel:LayoutEntity( Entity )
			if ( self.bAnimated ) then self:RunAnimation() end

			if ( self.Pressed ) then
				local mx, my = gui.MousePos()
				self.Angles = self.Angles - Angle( 0, ( self.PressX or mx ) - mx, 0 )
				
				frame.ModelRotation = self.Angles.y

				self.PressX, self.PressY = gui.MousePos()
			end

			Entity:SetAngles( self.Angles )
		end
		
		mainPanel.InfoPanel = vgui.Create("DPanel", mainPanel)
		mainPanel.InfoPanel:SetPos(10*scrw1 + totalw * (2 / 5) + 10*scrw1, 25*scrh1 + tabSize*scrh1 + 10*scrh1)
		mainPanel.InfoPanel:SetSize(totalw - (10*scrw1 + totalw * (2/5) + 10*scrw1 + totalw/3 - 50*scrw1), totalh - (25*scrh1 + tabSize*scrh1 + 20*scrh1))
		function mainPanel.InfoPanel:Paint(w, h)
		end
		mainPanel.InfoPanel:DockPadding(5, 0, 5, 5)
		
		mainPanel.InfoPanel.AppearanceList = vgui.Create("DPanelList", mainPanel.InfoPanel)
		mainPanel.InfoPanel.AppearanceList:Dock(BOTTOM)
		mainPanel.InfoPanel.AppearanceList:EnableVerticalScrollbar(true)
		mainPanel.InfoPanel.AppearanceList:EnableHorizontal(true)
		mainPanel.InfoPanel.AppearanceList:SetSpacing(5*scrh1)
		mainPanel.InfoPanel.AppearanceList:DockMargin(5*scrw1, 0, 5*scrw1, 5*scrh1)
		mainPanel.InfoPanel.AppearanceList.VBar:SetHideButtons(true)
		function mainPanel.InfoPanel.AppearanceList.VBar:Paint(w, h)
		end
		
		function mainPanel.InfoPanel.AppearanceList.VBar.btnGrip:Paint(w, h)
			surface.SetDrawColor(color_white)
			surface.DrawRect(2, 0, w - 2, h)
		end
		
		mainPanel.InfoPanel.AppearanceTitle = vgui.Create("DLabel", mainPanel.InfoPanel)
		mainPanel.InfoPanel.AppearanceTitle:SetFont("RashkinskRodchenko4")
		mainPanel.InfoPanel.AppearanceTitle:SetText("Внешность")
		mainPanel.InfoPanel.AppearanceTitle:SetTextColor(color_white)
		mainPanel.InfoPanel.AppearanceTitle:Dock(BOTTOM)
		mainPanel.InfoPanel.AppearanceTitle:DockMargin(5*scrw1, 0, 5*scrw1, 10*scrh1)
		
		
		
		mainPanel.InfoPanel.TextList = vgui.Create("DPanelList", mainPanel.InfoPanel)
		mainPanel.InfoPanel.TextList:Dock(FILL)
		mainPanel.InfoPanel.TextList:EnableVerticalScrollbar(true)
		mainPanel.InfoPanel.TextList:EnableHorizontal(false)
		mainPanel.InfoPanel.TextList:SetSpacing(5*scrh1)
		mainPanel.InfoPanel.TextList:DockMargin(5*scrw1, 0, 5*scrw1, 5*scrh1)
		mainPanel.InfoPanel.TextList.VBar:SetHideButtons(true)
		function mainPanel.InfoPanel.TextList.VBar:Paint(w, h)
		end
		
		function mainPanel.InfoPanel.TextList.VBar.btnGrip:Paint(w, h)
			surface.SetDrawColor(color_white)
			surface.DrawRect(2, 0, w - 2, h)
		end
		
		mainPanel.InfoPanel.DescriptionTitle = vgui.Create("DLabel", mainPanel.InfoPanel.TextList)
		mainPanel.InfoPanel.DescriptionTitle:SetFont("RashkinskRodchenko4")
		mainPanel.InfoPanel.DescriptionTitle:SetText("Описание")
		mainPanel.InfoPanel.DescriptionTitle:SetTextColor(color_white)
		mainPanel.InfoPanel.TextList:AddItem(mainPanel.InfoPanel.DescriptionTitle)
		//mainPanel.InfoPanel.DescriptionTitle:Dock(TOP)
		mainPanel.InfoPanel.DescriptionTitle:DockMargin(5*scrw1, 0, 5*scrw1, 5*scrh1)
		
		mainPanel.InfoPanel.DescriptionLabel = vgui.Create("DLabel", mainPanel.InfoPanel.TextList)
		mainPanel.InfoPanel.DescriptionLabel:SetFont("RashkinskFutura2")
		mainPanel.InfoPanel.DescriptionLabel:SetWrap(true)
		mainPanel.InfoPanel.DescriptionLabel:SetTextColor(color_white)
		mainPanel.InfoPanel.DescriptionLabel:SetAutoStretchVertical(true)
		mainPanel.InfoPanel.TextList:AddItem(mainPanel.InfoPanel.DescriptionLabel)
		mainPanel.InfoPanel.DescriptionLabel:SetText("")
		//mainPanel.InfoPanel.DescriptionLabel:Dock(TOP)
		mainPanel.InfoPanel.DescriptionLabel:DockMargin(5*scrw1, 0, 5*scrw1, 10*scrh1)
		
		mainPanel.InfoPanel.WeaponsTitle = vgui.Create("DLabel", mainPanel.InfoPanel.TextList)
		mainPanel.InfoPanel.WeaponsTitle:SetFont("RashkinskRodchenko4")
		mainPanel.InfoPanel.WeaponsTitle:SetText("Оружие")
		mainPanel.InfoPanel.WeaponsTitle:SetTextColor(color_white)
		mainPanel.InfoPanel.TextList:AddItem(mainPanel.InfoPanel.WeaponsTitle)
		//mainPanel.InfoPanel.WeaponsTitle:Dock(TOP)
		mainPanel.InfoPanel.WeaponsTitle:DockMargin(5*scrw1, 0, 5*scrw1, 5*scrh1)
		
		mainPanel.InfoPanel.WeaponsLabel = vgui.Create("DLabel", mainPanel.InfoPanel.TextList)
		mainPanel.InfoPanel.WeaponsLabel:SetFont("RashkinskFutura2")
		mainPanel.InfoPanel.WeaponsLabel:SetWrap(true)
		mainPanel.InfoPanel.WeaponsLabel:SetTextColor(color_white)
		mainPanel.InfoPanel.WeaponsLabel:SetAutoStretchVertical(true)
		mainPanel.InfoPanel.TextList:AddItem(mainPanel.InfoPanel.WeaponsLabel)
		mainPanel.InfoPanel.WeaponsLabel:SetText("")
		//mainPanel.InfoPanel.WeaponsLabel:Dock(TOP)
		mainPanel.InfoPanel.WeaponsLabel:DockMargin(5*scrw1, 0, 5*scrw1, 20*scrh1)
		
		
		
		local function fillInfoPanel()
			if selectedJob and RPExtraTeams[selectedJob] then
				local jobTable = RPExtraTeams[selectedJob]
				
				mainPanel.InfoPanel.DescriptionLabel:SetText(jobTable.description)
				
				local getWepName = fn.FOr{fn.FAnd{weapons.Get, fn.Compose{fn.Curry(fn.GetValue, 2)("PrintName"), weapons.Get}}, fn.Id}
				local getWeaponNames = fn.Curry(fn.Map, 2)(getWepName)
				local weaponString = fn.Compose{fn.Curry(fn.Flip(table.concat), 2)("\n"), fn.Curry(fn.Seq, 2)(table.sort), getWeaponNames, table.Copy}
		
				local weps = weaponString(jobTable.weapons)
				
				mainPanel.InfoPanel.WeaponsLabel:SetText(weps ~= "" and weps or DarkRP.getPhrase("no_extra_weapons"))
				mainPanel.InfoPanel.AppearanceList:Clear()
				local size = mainPanel.InfoPanel:GetWide() / 2 - 20*scrw1
				if istable(jobTable.model) then
					mainPanel.InfoPanel.AppearanceTitle:SetText("Внешность")
					local preferredModel = DarkRP.getPreferredJobModel(selectedJob) or jobTable.model[1]
					
					for k,v in pairs(jobTable.model) do
						local modelImage = vgui.Create("ModelImage", mainPanel.InfoPanel.AppearanceList)
						
						modelImage:SetSize(size, size)
						modelImage:SetModel(v)
						modelImage.model = v
						function modelImage:OnMouseReleased(mouseCode)
							if mouseCode == MOUSE_LEFT then
								DarkRP.setPreferredJobModel(selectedJob, v)
								preferredModel = v
								mainPanel.ModelPanel:SetModel(v, "idle_all_02")
								function mainPanel.ModelPanel.Entity:GetPlayerColor()
									return Vector(jobTable.color.r / 255, jobTable.color.g / 255, jobTable.color.b / 255)
								end
							elseif mouseCode == MOUSE_RIGHT then
								self:RebuildSpawnIcon()
							end
						end
						function modelImage:Paint(w, h)
							surface.SetDrawColor(color_white)
							surface.DrawRect(0, 0, w, h)
						end
						function modelImage:PaintOver(w, h)
							if self.model != preferredModel then
								surface.SetDrawColor(0, 0, 0, 200)
								surface.DrawRect(0, 0, w, h)
							end
						end
						mainPanel.InfoPanel.AppearanceList:AddItem(modelImage)
					end
				else
					mainPanel.InfoPanel.AppearanceTitle:SetText("")
				end
				
				mainPanel.InfoPanel.AppearanceList:SetTall(math.ceil(math.min(table.Count(mainPanel.InfoPanel.AppearanceList:GetItems()) / 2, 2)) * size)
				
				timer.Simple(FrameTime(), function()//Чтобы SetAutoStretchVertical успел изменить размер текста.
					if IsValid(mainPanel.InfoPanel.TextList) then
						mainPanel.InfoPanel.TextList:Rebuild()
					end
				end)
			end
		end
		
		
		mainPanel.JobsList = vgui.Create("DPanelList", mainPanel)
		mainPanel.JobsList:SetSize(totalw * (2 / 5), totalh - (25*scrh1 + tabSize*scrh1 + 20*scrh1))
		mainPanel.JobsList:SetPos(10*scrw1, 25*scrh1 + tabSize*scrh1 + 10*scrh1)
		function mainPanel.JobsList:Paint(w, h)
		end
		mainPanel.JobsList:SetSpacing(5*scrh1)
		mainPanel.JobsList:EnableVerticalScrollbar(true)
		mainPanel.JobsList.VBar:SetHideButtons(true)
		function mainPanel.JobsList.VBar:Paint(w, h)
		end
		
		function mainPanel.JobsList.VBar.btnGrip:Paint(w, h)
			surface.SetDrawColor(color_white)
			surface.DrawRect(2*scrw1, 0, w - 2*scrw1, h)
		end
		
		local jobCategories = DarkRP.getCategories().jobs
		for k,v in pairs(jobCategories) do
			local cantSee = #v.members == 0 or isfunction(v.canSee) and not v.canSee(LocalPlayer())
			if !cantSee then
				local category = vgui.Create("DCollapsibleCategory", mainPanel.JobsList)
				category.Header:SetTall(30)
				--category.Header:SetEnabled(false)//УДАЛИТЕ ЭТУ СТРОКУ ЧТОБЫ СДЕЛАТЬ КАТЕГОРИИ ЗАКРЫВАЮЩИМИСЯ
				function category:Paint(w,h)
					
				end
				function category.Header:Paint(w,h)
					draw.SimpleText(v.name, "RashkinskRodchenko4", 10*scrw1, h/2, Color(255,255,255), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
				end
				category:SetLabel("")
				local contents = vgui.Create("DPanelList", category)
				contents:SetPadding(8)
				contents:SetSpacing(8*scrh1)
				category:SetContents(contents)
				
				for k,v in pairs(v.members) do
					local canGet, blocked = canGetJob(v)
					if !blocked then
						local jobButton = vgui.Create("DButton", contents)
						jobButton:SetText("")
						jobButton:SetTall(50*scrh1)
						local jobColorWidth = 6
						function jobButton:Paint(w, h)
							surface.SetDrawColor(self:IsEnabled() and color_white or Rashkinsk.UIColors.LightGrey)
							surface.DrawRect(0, 0, w, h)
							if self:IsEnabled() and !self.Hovered and !self.Depressed and v.team != selectedJob then
								surface.SetMaterial(Rashkinsk.UIMaterials.grad)
								surface.DrawTexturedRect(0, 0, w, h)
								surface.DrawTexturedRect(0, 0, w, h)
							end
							
							surface.SetDrawColor(v.color)
							surface.DrawRect(w - jobColorWidth, 0, jobColorWidth, h)
							
							draw.SimpleText(v.name, "RashkinskRodchenko4", w/2, h/2, color_black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
							local text = tostring(v.max)
							if text == "0" then
								text = "∞"
							end
							text = team.NumPlayers(v.team).."/"..text
							
							draw.SimpleText(text, "RashkinskRodchenko4", w - jobColorWidth - 4*scrw1 - 5*scrw1, h/2, (v.max > 0 and v.max <= team.NumPlayers(v.team)) and Rashkinsk.UIColors.BrightRed or color_black, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)
						end
						jobButton:SetEnabled(canGet)
						function jobButton:DoClick()
							selectedJob = v.team
							local model = DarkRP.getPreferredJobModel(v.team) or istable(v.model) and v.model[1] or v.model
							mainPanel.ModelPanel:SetModel(model, "idle_all_02")
							function mainPanel.ModelPanel.Entity:GetPlayerColor()
								return Vector(v.color.r / 255, v.color.g / 255, v.color.b / 255)
							end
							fillInfoPanel()
						end
						jobButton.ModelImage = vgui.Create("ModelImage", jobButton)
						jobButton.ModelImage:SetModel(istable(v.model) and v.model[1] or v.model)
						jobButton.ModelImage:SetSize(50*scrh1, 50*scrh1)
						jobButton.ModelImage.DoClick = jobButton.DoClick
						
						function jobButton:DoRightClick()
							self.ModelImage:RebuildSpawnIcon()
						end
						
						if !selectedJob and canGet then
							jobButton:DoClick()
						end
						contents:AddItem(jobButton)
					end
				end
				
				mainPanel.JobsList:AddItem(category)
			end
		end
		
		mainPanel.BecomeJob = vgui.Create("DButton", mainPanel)
		
		
		local infoX, infoY = mainPanel.InfoPanel:GetPos()
		mainPanel.BecomeJob:SetSize(totalw - infoX - mainPanel.InfoPanel:GetWide() - 30*scrw1, 50*scrh1)
		mainPanel.BecomeJob:SetPos(infoX + mainPanel.InfoPanel:GetWide(), totalh - 80*scrh1)
		mainPanel.BecomeJob:SetText("")
		function mainPanel.BecomeJob:Paint(w, h)
			if selectedJob and RPExtraTeams[selectedJob] then
				surface.SetDrawColor(color_white)
				surface.DrawRect(0, 0, w, h)
				if !self.Hovered and !self.Depressed then
					surface.SetMaterial(Rashkinsk.UIMaterials.grad)
					surface.DrawTexturedRect(0, 0, w, h)
					surface.DrawTexturedRect(0, 0, w, h)
				end
				local text = "Выбрать"
				local jobTable = RPExtraTeams[selectedJob]
				if jobTable.vote or jobTable.RequiresVote and jobTable.RequiresVote(ply, ply:Team()) then
					text = "Начать голосование"
				end
				draw.SimpleText(text, "RashkinskRodchenko4", w/2, h/2, color_black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			end
		end
		function mainPanel.BecomeJob:DoClick()
			if selectedJob and RPExtraTeams[selectedJob] then
				local jobTable = RPExtraTeams[selectedJob]
				if jobTable.vote or jobTable.RequiresVote and jobTable.RequiresVote(ply, ply:Team()) then
					RunConsoleCommand("darkrp", "vote"..jobTable.command)
				else
					RunConsoleCommand("darkrp", jobTable.command)
				end
				frame:Close()
			end
		end
		
		return mainPanel
	end)
	AddTab("Предметы", Rashkinsk.UIMaterials.money, function(frame)
		local mainPanel = vgui.Create("DPanel", frame)
		mainPanel:SetPos(0, 0)
		mainPanel:SetSize(totalw, totalh)
		
		function mainPanel:Paint(w, h)
		end
		
		mainPanel:MoveToBack()
		
		mainPanel.Sheet = vgui.Create("DPropertySheet", mainPanel)
		mainPanel.Sheet:Dock(TOP)
		mainPanel.Sheet:DockMargin(0,90*scrh1,0,0)
		mainPanel.Sheet:SetPadding(30*scrh1)
		mainPanel.Sheet:SetSize(totalw - 20*scrw1, totalh - 45*scrh1 - tabSize*scrh1)
		function mainPanel.Sheet:Paint(w, h)
		end
		
		local function loadItems(name, categories, action, canBuyFunction)
			local panel = vgui.Create("DPanel", mainPanel.Sheet)
			function panel:Paint(w, h)
			end
			
			local itemsList = vgui.Create("DPanelList", panel)
			itemsList:Dock(FILL)
			itemsList:EnableVerticalScrollbar(true)
			itemsList.VBar:SetHideButtons(true)
			function itemsList.VBar:Paint(w, h)
			end
			function itemsList.VBar.btnGrip:Paint(w, h)
				surface.SetDrawColor(color_white)
				surface.DrawRect(2*scrw1, 0, w - 2*scrw1, h)
			end
			
			local shouldKeep = false
			
			for k,v in pairs(categories) do
				local cantSee = #v.members == 0 or isfunction(v.canSee) and not v.canSee(LocalPlayer())
				if !cantSee then
					local label = vgui.Create("DLabel", itemsList)
					
					label:SetFont("RashkinskRodchenko6")
					label:SetTextColor(color_white)
					local name = v.name
					if name == "Other" then
						name = "Разное"
					end
					label:SetText(name)
					label:SizeToContents()
					itemsList:AddItem(label)
					local shouldKeepCategory = false
					local contents = vgui.Create("DPanelList", itemsList)
					contents:EnableHorizontal(true)
					contents:EnableVerticalScrollbar(false)
					contents:SetSpacing(5*scrh1)
					contents:SetAutoSize(true)
					itemsList:AddItem(contents)
					for k,v in pairs(v.members) do
						local could, important, _, price = canBuyFunction(v)
						if !important then
							shouldKeepCategory = true
							shouldKeep = true
							local modelimage = vgui.Create("ModelImage", contents)
							modelimage:SetSize(ScrH()*.2, ScrH()*.2)
							modelimage:SetModel(v.model)
							function modelimage:Paint(w,h)
								surface.SetDrawColor(color_white)
								surface.DrawRect(0,0,w,h)
							end
							local button = vgui.Create("DButton", modelimage)
							button:SetSize(ScrH()*.2,ScrH()*.2)
							button:SetText("")
							
							function button:DoClick()
								action(v)
							end
							function button:Paint(w,h)
								local borderColor = Color(68, 68, 68, 230)
								if !could then
									borderColor = Rashkinsk.UIColors.Red
								end
								surface.SetDrawColor(borderColor)
								surface.DrawOutlinedRect(0,0,w,h)
								surface.DrawRect(1*scrw1,h*.75,w-2*scrw1,h*.75)
								draw.DrawText(v.name.."\n"..DarkRP.formatMoney(price) ,"RashkinskFutura1",w/2, h*.74, color_white, TEXT_ALIGN_CENTER)
							end
							contents:AddItem(modelimage)
						end
					end
					
					if !shouldKeepCategory then
						contents:Remove()
						label:Remove()
					end
				end
				
			end
			if shouldKeep then
				local sheetData = mainPanel.Sheet:AddSheet(name, panel)
				local tab = sheetData.Tab
				function tab:Paint(w, h)
				end
				tab:SetFont("RashkinskRodchenko6")
				function tab:GetTabHeight()
					return 40*scrh1
				end
			else
				panel:Remove()
			end
		end
		
		local cats = DarkRP.getCategories().entities
		loadItems("Предметы", cats, function(item) RunConsoleCommand("DarkRP", item.cmd) end, canBuyEntity)
		local cats = DarkRP.getCategories().weapons
		loadItems("Оружие", cats, function(item) RunConsoleCommand("DarkRP", "buy", item.name) end, canBuyGun)
		local cats = DarkRP.getCategories().shipments
		loadItems("Поставки",cats, function(item) RunConsoleCommand("DarkRP", "buyshipment", item.name) end, canBuyShipment)
		
		if FoodItems then
			local newtable = {
				[1] = {
					name = "Еда",
					members = FoodItems,
				}
			}
			loadItems("Еда", newtable, function(item) RunConsoleCommand("DarkRP", "buyfood", item.name) end, canBuyFood)
		end
		
		local cats = DarkRP.getCategories().ammo
		loadItems("Патроны", cats, function(item) RunConsoleCommand("DarkRP", "buyammo", item.id) end, canBuyAmmo)
		
		local cats = DarkRP.getCategories().vehicles
		loadItems("Транспорт", cats, function(item) RunConsoleCommand("DarkRP", "buyvehicle", item.name) end, canBuyVehicle)
		
		
		return mainPanel
	end)

end)