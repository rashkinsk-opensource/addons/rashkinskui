	local lerpHealth = 0
	local lerpArmor = 0
	local lerpEnergy = 0
	
	local agendaText
	
	local screenwidth = ScrW()
	local screenheight = ScrH()

	local scrw1 = 1/1600*screenwidth
	local scrh1 = 1/900*screenheight
	
	local agendaWidth = ScrW()*.23
	local initialAgendaHeight = ScrH()*.15
	local agendaHeight = initialAgendaHeight
	local agendaTitleHeight = scrh1*27
	print(ScrW().." - "..ScrH())
	
	include( "autorun/wanted_config.lua" )
	/*
	Screen 1360 - 768
	
	10 = screenwidth * 0.0073529
	10 = screenheight * 0.0130208
	
	90 = screenwidth * 0.06617
	90 = screenheight * 0.11718
	
	*/
	
	hook.Add("DarkRPVarChanged", "UpdateRashkinskAgenda", function(ply, var, _, new)
		if ply == LocalPlayer() then
			if var == "agenda" then
				if new then
					agendaText = DarkRP.textWrap(new:gsub("//", "\n"):gsub("\\n", "\n"), "RashkinskFutura1", agendaWidth - scrw1*20)
					surface.SetFont("RashkinskFutura1")
					local w, h = surface.GetTextSize(agendaText)
					agendaHeight = math.max(initialAgendaHeight, agendaTitleHeight + h + scrh1 * 10)
				else
					agendaText = nil
				end
			end
		end
	end)
	
	
	
	local function cutFormAgenda()
		Rashkinsk.RoundedBoxEx(4, 0, 0, agendaWidth, agendaHeight, color_white, false, true, false, false)
	end
	
	local function drawAgendaItself(text, title)
		surface.SetDrawColor(Rashkinsk.UIColors.Red)
		--surface.SetMaterial(Rashkinsk.UIMaterials.grad)
		surface.DrawRect(0, 0, agendaWidth, agendaTitleHeight)
		surface.DrawTexturedRect(0, 0, agendaWidth, agendaTitleHeight)
		surface.DrawTexturedRect(0, 0, agendaWidth, agendaTitleHeight)
		surface.DrawTexturedRect(0, 0, agendaWidth, agendaTitleHeight)
		if text != "" then
			surface.SetDrawColor(Color(50, 50, 50,100))
			surface.DrawRect(0, agendaTitleHeight, agendaWidth, agendaHeight - agendaTitleHeight)
			draw.SimpleText(title, "RashkinskRodchenko9", scrw1*10, agendaTitleHeight/2, color_white, TEXT_ALIGN_LEFT, 1)
		else
			draw.SimpleText(title, "RashkinskRodchenko9", scrw1*10, agendaTitleHeight/2, color_white, TEXT_ALIGN_LEFT, 1)
		end
		
		draw.DrawText(text, "RashkinskFutura9", scrw1*10, agendaTitleHeight+scrh1 * 10, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
	end
	
	local function drawAgenda()
		local ply = LocalPlayer()
		local agendaData = ply:getAgendaTable()
		if agendaData then
			if !agendaText then
				agendaText = agendaText or DarkRP.textWrap((ply:getDarkRPVar("agenda") or ""):gsub("//", "\n"):gsub("\\n", "\n"), "RashkinskFutura1", agendaWidth - scrw1*20)
				surface.SetFont("RashkinskFutura9")
				local w, h = surface.GetTextSize(agendaText)
				agendaHeight = math.max(initialAgendaHeight, agendaTitleHeight + h + scrh1*10)
			end
			Rashkinsk.DrawMasked(cutFormAgenda, function() drawAgendaItself(agendaText, agendaData.Title) end)
		end
	end

	local function LockDown()
	    local chbxX, chboxY = chat.GetChatBoxPos()
	    if GetGlobalBool("DarkRP_LockDown") then
	        local shouldDraw = hook.Call("HUDShouldDraw", GAMEMODE, "DarkRP_LockdownHUD")
	        if shouldDraw == false then return end
	        local cin = (math.sin(CurTime()) + 1) / 2
	        local chatBoxSize = math.floor(ScrH() / 4)
	        draw.DrawNonParsedText(DarkRP.getPhrase("lockdown_started"), "ScoreboardSubtitle", chbxX, chboxY + chatBoxSize, Color(cin * 255, 0, 255 - (cin * 255), 255), TEXT_ALIGN_LEFT)
	    end
	end
	
	local function DisplayNotify(msg)
		local txt = msg:ReadString()
		GAMEMODE:AddNotify(txt, msg:ReadShort(), msg:ReadLong())
		surface.PlaySound("buttons/lightswitch2.wav")

		MsgC(Color(255, 20, 20, 255), "[DarkRP] ", Color(200, 200, 200, 255), txt, "\n")
	end
	usermessage.Hook("_Notify", DisplayNotify)
	
	local AdminTell = function() end
	timer.Simple(1, function()
		usermessage.Hook("AdminTell", function(msg)
			timer.Remove("DarkRP_AdminTell")
			local Message = msg:ReadString()

			AdminTell = function()
				draw.RoundedBox(4, 10, 10, screenwidth - 20, 110, Color(0,0,0,175))
				draw.DrawNonParsedText(DarkRP.getPhrase("listen_up"), "GModToolName", screenwidth / 2 + 10, 10, Color(255,255,255), 1)
				draw.DrawNonParsedText(Message, "ChatFont", screenwidth / 2 + 10, 90, Color(255,255,255), 1)
			end

			timer.Create("DarkRP_AdminTell", 10, 1, function()
				AdminTell = function() end
			end)
		end)
	end)
	
	local starmaterial = Material("icon16/star.png")
	
	local function PaintHUD()

		local tr = LocalPlayer():GetEyeTrace()

		if IsValid(tr.Entity) and tr.Entity:isKeysOwnable() and tr.Entity:GetPos():DistToSqr(LocalPlayer():GetPos()) < 40000 then
			tr.Entity:drawOwnableInfo()
		end
	
		if LocalPlayer():Alive() then
		drawAgenda()
		LockDown()

		--[[ if LocalPlayer():IsSpeaking() then
			draw.SimpleText("Вы говорите","RashkinskFutura9",16,ScrH()/2+1,Color(0,0,0))
			draw.SimpleText("Вы говорите","RashkinskFutura9",15,ScrH()/2,Color(230,40,40))
		end ]]

		AdminTell()

		local shouldPaintHUD = hook.Run("HUDShouldDraw", "RashkinskHUD")
			if shouldPaintHUD then
				local scrw = ScrW()
				local scrh = ScrH()
				local ply = LocalPlayer()

				surface.SetFont( "RashkinskFutura9" )
				local jw, _ = surface.GetTextSize(ply:getDarkRPVar("job"))
				local salary = DarkRP.formatMoney(LocalPlayer():getDarkRPVar("salary")) or ""
				local sw, _ = surface.GetTextSize(salary)
				local money = DarkRP.formatMoney(LocalPlayer():getDarkRPVar("money")) or ""
				local mw, _ = surface.GetTextSize(money)

				surface.SetFont( "RashkinskRodchenko9" )
				local jsw, _ = surface.GetTextSize("Работа:")
				local ssw, _ = surface.GetTextSize("Кошелек:")
				local dwst = jw+jsw+sw+ssw+scrw1*30

				local hudw = math.Clamp(dwst,350,dwst)
				local hudh = scrh1*100
				draw.RoundedBox(0, 0, scrh - hudh, hudw, hudh,Rashkinsk.UIColors.Grey)
				surface.SetDrawColor(Rashkinsk.UIColors.Red)
				surface.SetMaterial(Rashkinsk.UIMaterials.bghud)
				surface.DrawRect(0, scrh - hudh + scrh1*61, hudw, hudh)

				--health

				lerpHealth = Lerp(FrameTime() * 5, lerpHealth, math.Clamp(ply:Health() / (ply:GetMaxHealth() or 100), 0, 1))
				
				surface.SetDrawColor(Rashkinsk.UIColors.Grey)
				surface.SetMaterial(Rashkinsk.UIMaterials.grad)
				surface.DrawRect(0, scrh - hudh, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh, hudw, scrh1*21)

				surface.SetDrawColor(Color(176, 22, 31))
				surface.DrawRect(0, scrh - hudh, hudw * lerpHealth, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh, hudw * lerpHealth, scrh1*21)
				draw.SimpleText(math.Round(math.Clamp(ply:Health() or "0",0,2147483647)), "RashkinskFutura9", hudw/2, scrh - hudh + scrh1*21/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				
				surface.SetDrawColor(color_white)
				surface.SetMaterial(Rashkinsk.UIMaterials.health)
				surface.DrawTexturedRect(scrw1*10, scrh - hudh + scrh1*3, scrh1*14, scrh1*14)
		
				--energy
				
				lerpEnergy = Lerp(FrameTime() * 5, lerpEnergy, math.Clamp((ply:getDarkRPVar("Energy") or 0) / 100, 0, 1))
				
				surface.SetDrawColor(Rashkinsk.UIColors.Grey)
				surface.SetMaterial(Rashkinsk.UIMaterials.grad)
				surface.DrawRect(0, scrh - hudh + scrh1*20, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*20, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*20, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*20, hudw, scrh1*21)
				
				surface.SetDrawColor(Color(49,196,98))
				surface.DrawRect(0, scrh - hudh + scrh1*20, hudw * lerpEnergy, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*20, hudw * lerpEnergy, scrh1*21)
				draw.SimpleText(math.Round(ply:getDarkRPVar("Energy") or "0"), "RashkinskFutura9", hudw/2, scrh - hudh + scrh1*20 + scrh1*21/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				
				surface.SetDrawColor(color_white)
				surface.SetMaterial(Rashkinsk.UIMaterials.food)
				surface.DrawTexturedRect(scrw1*10, scrh - hudh + scrh1*20 + scrh1*3, scrh1*14, scrh1*14)
				
				--armor

				lerpArmor = Lerp(FrameTime() * 5, lerpArmor, math.Clamp(ply:Armor() / 100, 0, 1))
				
				surface.SetDrawColor(Rashkinsk.UIColors.Grey)
				surface.SetMaterial(Rashkinsk.UIMaterials.grad)
				surface.DrawRect(0, scrh - hudh + scrh1*40, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*40, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*40, hudw, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*40, hudw, scrh1*21)
				
				surface.SetDrawColor(Color(29, 112, 182))
				surface.DrawRect(0, scrh - hudh + scrh1*40, hudw * lerpArmor, scrh1*21)
				surface.DrawTexturedRect(0, scrh - hudh + scrh1*40, hudw * lerpArmor, scrh1*21)
				draw.SimpleText(math.Round(math.Clamp(ply:Armor() or "0",0,2147483647)), "RashkinskFutura9", hudw/2, scrh - hudh + scrh1*40 + scrh1*21/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				
				surface.SetDrawColor(color_white)
				surface.SetMaterial(Rashkinsk.UIMaterials.armor)
				surface.DrawTexturedRect(scrw1*10, scrh - hudh + scrh1*40 + scrh1*3, scrh1*14, scrh1*14)

				local haslicense = ply:getDarkRPVar("HasGunlicense") or false
				
				surface.SetDrawColor(haslicense and color_white or color_black)
				surface.SetMaterial(Rashkinsk.UIMaterials.weaponwarrant)
				surface.DrawTexturedRect(hudw - scrw1*40, scrh -scrh1*35, scrh1*15, scrh1*15)
				
				local iswanted = ply:getDarkRPVar("wanted") or false
				
				surface.SetDrawColor(iswanted and color_white or color_black)
				surface.SetMaterial(Rashkinsk.UIMaterials.wanted)
				surface.DrawTexturedRect(hudw - scrw1*40/2, scrh - scrh1*35, scrh1*15, scrh1*15)


				draw.SimpleText("Работа: "..ply:getDarkRPVar("job"), "RashkinskRodchenko9", scrw1*10, scrh - scrh1*20, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_BOTTOM)
				--draw.SimpleText(ply:getDarkRPVar("job") or "", "RashkinskFutura9", scrw1*75, scrh - scrh1*20, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_BOTTOM)


				
				local salary = DarkRP.formatMoney(LocalPlayer():getDarkRPVar("salary")) or ""
				--[[ local ws, _ = surface.GetTextSize(salary)
				draw.SimpleText("Зарплата:", "RashkinskRodchenko9", hudw - sw - scrw1*10/2, scrh - scrh1*2, color_white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM)
				draw.SimpleText(salary, "RashkinskFutura9", hudw - scrw1*10/2, scrh - scrh1*2, color_white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM) ]]
				
				local money = DarkRP.formatMoney(ply:getDarkRPVar("money")) or ""
				
				surface.SetFont("RashkinskFutura9")
				
				draw.SimpleText("Кошелек: "..money.." (+"..salary..")", "RashkinskRodchenko9", scrw1*10, scrh - scrh1*2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_BOTTOM)
				--draw.SimpleText(money.." (+"..salary..")", "RashkinskFutura9", ssw*scrw1 + scrw1*8, scrh - scrh1*2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_BOTTOM)

				
				
				if GetWantedLevel(LocalPlayer()) > 0 then
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
					surface.DrawTexturedRect( 16*scrw1, scrh - hudh - 30*scrh1, 25*scrh1, 25*scrh1 )
				end
				if GetWantedLevel(LocalPlayer()) > 1 then
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
					surface.DrawTexturedRect( 16*scrw1*2 + 25*scrw1, scrh - hudh - 30*scrh1, 25*scrh1, 25*scrh1 )
				end
				if GetWantedLevel(LocalPlayer()) > 2 then
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
					surface.DrawTexturedRect( 16*scrw1*3 + 25*scrw1*2, scrh - hudh - 30*scrh1, 25*scrh1, 25*scrh1 )
				end
				if GetWantedLevel(LocalPlayer()) > 3 then
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
					surface.DrawTexturedRect( 16*scrw1*4 + 25*scrw1*3, scrh - hudh - 30*scrh1, 25*scrh1, 25*scrh1 )
				end
				if GetWantedLevel(LocalPlayer()) > 4 then
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
					surface.DrawTexturedRect( 16*scrw1*5 + 25*scrw1*4, scrh - hudh - 30*scrh1, 25*scrh1, 25*scrh1 )
				end
				

			end
		end
	end
	
	hook.Add("HUDPaint", "RashkinskHUDPaint", PaintHUD)
	
	-- Отключение стандартных элементов GMod и Dark
	local hidden = { "DarkRP_ArrestedHUD","DarkRP_EntityDisplay", "DarkRP_LocalplyayerHUD", "DarkRP_HUD", "DarkRP_Hungermod", "CHudHealth"}
	hook.Add("HUDShouldDraw", "SHud_Hide", function(name)
		if table.HasValue(hidden, name) || (name == "DarkRP_EntityDisplyay") then return false end
	end)
	function hidehud(name)
		for k, v in pairs({"CHudHealth", "CHudBattery"})do
			if name == v then return false end
		end
	end
	hook.Add("HUDShouldDraw", "HideOurHud", hidehud)

local camS=cam.Start3D2D
local camE=cam.End3D2D
local playerGA=player.GetAll
local drawST=draw.SimpleText
local teamGC=team.GetColor
local abs,sin,floor,Round = math.abs,math.sin,math.floor,math.Round
local curtime = CurTime
local Color=Color
local Vector=Vector
local Angle=Angle
local surface=surface

local eyepos
 
surface.CreateFont("3D2DNicksNickFont", {
	font = "RodchenkoCTT",
	size = 500/6,
	weight = 800,
	antialias = true,
	additive = true,
})
 
surface.CreateFont("3D2DNicksNickFont_Blur", {
	font = "RodchenkoCTT",
	size = 500/6,
	weight = 800,
	antialias = true,
	additive = false,
	blursize = 8,
})
 
surface.CreateFont("3D2DNicksNickFont2", {
	font = "RodchenkoCTT",
	size = 120/6,
	weight = 800,
	antialias = true,
	additive = true,
})
 
surface.CreateFont("3D2DNicksNickFont_Blur2", {
	font = "RodchenkoCTT",
	size = 120/6,
	weight = 800,
	antialias = true,
	additive = false,
	blursize = 8,
})

surface.CreateFont( "TextHUDOverhide", {
        font = "Futura-Normal",
        size = 50,
        weight = 700,
        antialias = true,
})	

local drawables = {}
hook.Add("PostPlayerDraw", "NameTags", function(pl) drawables[pl] = true end)

hook.Add("RenderScene", "3D2DNicksPosAng",function(pos) eyepos = pos end)

local function DrawText(txt,color,x,y)
	for i=1,2 do
		drawST(txt, "3D2DNicksNickFont_Blur", x, y, Color(0,0,0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	drawST(txt, "3D2DNicksNickFont", x, y, color,TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
end

hook.Add("PostDrawTranslucentRenderables", "NameTags", function()


	for v,ply in pairs(playerGA()) do
		if drawables[ply] then
			drawables[ply] = false
			
			local dist = ply:GetPos():Distance(eyepos)
			if dist < 350 and ply:Alive() then
				local bone = ply:LookupAttachment("eyes")
				if bone != 0 then			
					local attach = ply:GetAttachment(bone)
					
					local job_color = teamGC(ply:Team())
					local color = teamGC(ply:Team())
				   
					local psn = ply:GetNWVector("PS_NickColor")
					
					if ply:GetNWVector("PS_NickColor") then
						local r,g,b = psn[1],psn[2],psn[3]	
						if r~=0 or g~=0 or b~=0 then
							color = Color(r,g,b)	
						end
					end
				
					camS(attach.Pos+Vector(0,0,16), Angle(0, (attach.Pos - eyepos):Angle().y - 90,90), 0.035)

						if ply:GetNWString("orgName") ~= "" then
							minus_pos_hud = 0
							DrawText(ply:GetNWString("orgName"),Color(255,0,0),0,200)
						else
							minus_pos_hud = 120
						end

   						DrawText(ply:GetName(),Color(255,255,255),0,-65+minus_pos_hud)
						DrawText(ply:getDarkRPVar("job"),job_color,0,75+minus_pos_hud)

						poswanted = -400+minus_pos_hud
				
						if ply:getDarkRPVar("wanted") and GetWantedVisible(ply) then
							if LocalPlayer():isCP() then
								DrawText("В розыске: "..(ply:getDarkRPVar("wantedReason") or ""),Color(255, 0, 0),0,poswanted)
							else
								DrawText("В розыске!",Color(255, 0, 0),0,poswanted+200)
							end
						end
						
						if LocalPlayer():isCP() and GetWantedVisible(ply) then
							if GetWantedLevel(ply) == 1 then
								surface.SetDrawColor( 255, 255, 0, 255 )
								surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
								surface.DrawTexturedRect( 0*scrw1-50*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
							end
							if GetWantedLevel(ply) == 2 then
								surface.SetDrawColor( 255, 255, 0, 255 )
								surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
								surface.DrawTexturedRect( 0*scrw1-125*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1+25*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
							end
							if GetWantedLevel(ply) == 3 then
								surface.SetDrawColor( 255, 255, 0, 255 )
								surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
								surface.DrawTexturedRect( 0*scrw1-50*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1-50*scrw1-50*scrw1-100*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1+100*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
							end
							if GetWantedLevel(ply) == 4 then
								surface.SetDrawColor( 255, 255, 0, 255 )
								surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
								surface.DrawTexturedRect( 0*scrw1-125*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1+25*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1-275*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1+175*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
							end
							if GetWantedLevel(ply) == 5 then
								surface.SetDrawColor( 255, 255, 0, 255 )
								surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
								surface.DrawTexturedRect( 0*scrw1-50*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1-200*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1+100*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1-350*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
								surface.DrawTexturedRect( 0*scrw1+250*scrw1, -150*scrh1, 100*scrw1, 100*scrh1 )
							end
						end

						if ply:getDarkRPVar("HasGunlicense") and !ply:isArrested() then
							draw.SimpleText("Имеется лицензия","TextHUDOverhide",0,-25+minus_pos_hud,Color(255,255,255),TEXT_ALIGN_CENTER)
						elseif ply:isArrested() then
							draw.SimpleText("Арестован","TextHUDOverhide",0,-25+minus_pos_hud,Color(255,0,0),TEXT_ALIGN_CENTER)
						end
					
					camE()
				end
			end
		end
	end
end)