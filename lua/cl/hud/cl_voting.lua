local QuestionVGUI = {}
local PanelNum = 0
local VoteVGUI = {}

local scrw1 = 1/1350*ScrW()
local scrh1 = 1/768*ScrH()

local votew = 150
local voteh = 125

local questionw = 280
local questionh = 125

print("vote loaded")

local function MsgDoVote(msg)
    local _, chatY = chat.GetChatBoxPos()

    local question = msg:ReadString()
    local voteid = msg:ReadShort()
    local timeleft = msg:ReadFloat()
    if timeleft == 0 then
        timeleft = 100
    end
    local OldTime = CurTime()
    if not IsValid(LocalPlayer()) then return end -- Sent right before player initialisation
	local title = "Голосование: "

    LocalPlayer():EmitSound("Town.d1_town_02_elevbell1", 100, 100)
    local panel = vgui.Create("DFrame")
    panel:SetPos(scrw1*5 + PanelNum, chatY - 145*scrh1)
	panel:SetTitle("")
    panel:SetSize(scrw1*votew, scrh1*voteh)
    panel:SetSizable(false)
    panel.btnClose:SetVisible(false)
    panel.btnMaxim:SetVisible(false)
    panel.btnMinim:SetVisible(false)
    panel:SetDraggable(false)
	panel.Paint = function (self,w,h)
			surface.SetDrawColor(Rashkinsk.UIColors.Grey)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(Rashkinsk.UIColors.Red)
			--surface.SetMaterial(Rashkinsk.UIMaterials.grad)
			surface.DrawRect(0, 0, w, scrh1*25)
			--surface.DrawTexturedRect(0, 0, w, 30)
			--surface.DrawTexturedRect(0, 0, w, 30)
			--surface.DrawTexturedRect(0, 0, w, 30)

			draw.SimpleText(title,"RashkinskRodchenkoVote",scrw1*10,scrh1*7,color_white)
	end
    function panel:Close()
        --PanelNum = PanelNum - scrw1*votew
		PanelNum = 0
        VoteVGUI[voteid .. "vote"] = nil

        local num = 0
        for k,v in SortedPairs(VoteVGUI) do
            v:SetPos(num, ScrH() / 2 - 50)
            num = num + scrw1*votew
        end

        for k,v in SortedPairs(QuestionVGUI) do
            v:SetPos(num, ScrH() / 2 - 50)
            num = num + scrw1*votew
        end
        self:Remove()
    end

    function panel:Think()
        title = "Голосование: "..math.Clamp(math.ceil(timeleft - (CurTime() - OldTime)), 0, 9999)
        if timeleft - (CurTime() - OldTime) <= 0 then
            panel:Close()
        end
    end

    panel:SetKeyboardInputEnabled(false)
    panel:SetMouseInputEnabled(true)
    panel:SetVisible(true)

	for i = 31, string.len(question), 31 do
        if not string.find(string.sub(question, i - 29, i), "\n", 1, true) then
            question = string.sub(question, 1, i) .. "\n" .. string.sub(question, i + 1, string.len(question))
        end
    end

    local label = vgui.Create("DLabel")
    label:SetParent(panel)
    label:SetPos(scrw1*5, scrh1*30)
    label:SetText(DarkRP.deLocalise(question))
	label:SetSize(scrw1*(votew-10), scrh1*(voteh-40))
    --label:SizeToContents()
	label:SetFont("RashkinskFuturaSmall")
    label:SetVisible(true)

    --[[ local nextHeight = label:GetTall() > 78 and label:GetTall() - 78 or 0 -- Make panel taller for divider and buttons
    panel:SetTall(panel:GetTall() + nextHeight) ]]


    local ybutton = vgui.Create("DImageButton")
    ybutton:SetParent(panel)
    ybutton:SetPos(scrw1*votew-scrw1*24*2-scrw1*10*2, panel:GetTall() - scrh1*35)
    ybutton:SetSize( 15*scrw1, 15*scrh1 )			// OPTIONAL: Use instead of SizeToContents() if you know/want to fix the size
	ybutton:SetImage( Rashkinsk.UIMaterials.check )
    ybutton:SetCommand("!")
    ybutton:SetVisible(true)
    ybutton.DoClick = function()
        LocalPlayer():ConCommand("vote " .. voteid .. " yea\n")
        panel:Close()
    end

    local nbutton = vgui.Create("DImageButton")
    nbutton:SetParent(panel)
    nbutton:SetPos(scrw1*votew-scrw1*24-scrw1*10, panel:GetTall() - scrh1*35)
    nbutton:SetSize( 15*scrw1, 15*scrh1 )			// OPTIONAL: Use instead of SizeToContents() if you know/want to fix the size
	nbutton:SetImage( Rashkinsk.UIMaterials.cross )
    nbutton:SetCommand("!")
    nbutton:SetVisible(true)
    nbutton.DoClick = function()
        LocalPlayer():ConCommand("vote " .. voteid .. " nay\n")
        panel:Close()
    end

    --PanelNum = PanelNum + scrw1*votew + scrw1*3
	PanelNum = 0
    VoteVGUI[voteid .. "vote"] = panel
    --panel:SetSkin(GAMEMODE.Config.DarkRPSkin)
	hook.Add("ShowHelp","VoteAnswerYes",function() ybutton.DoClick() print("Проголосовали ЗА") return true end)
	hook.Add("ShowTeam","VoteAnswerNo",function() nbutton.DoClick() print("Проголосовали ПРОТИВ") return true end)
end
usermessage.Hook("DoVote", MsgDoVote)

local function KillVoteVGUI(msg)
    local id = msg:ReadShort()

    if VoteVGUI[id .. "vote"] and VoteVGUI[id .. "vote"]:IsValid() then
        VoteVGUI[id .. "vote"]:Close()
    end
end
usermessage.Hook("KillVoteVGUI", KillVoteVGUI)

local function MsgDoQuestion(msg)
	 local _, chatY = chat.GetChatBoxPos()
    local question = msg:ReadString()
    local quesid = msg:ReadString()
    local timeleft = msg:ReadFloat()
    if timeleft == 0 then
        timeleft = 100
    end
    local OldTime = CurTime()
    if not IsValid(LocalPlayer()) then return end -- Sent right before player initialisation
	local title = "Голосование: "

    LocalPlayer():EmitSound("Town.d1_town_02_elevbell1", 100, 100)
    local panel = vgui.Create("DFrame")
    panel:SetPos(scrw1*5 + PanelNum, chatY - 145*scrh1)
	panel:SetTitle("")
    panel:SetSize(scrw1*questionw, scrh1*questionh)
    panel:SetSizable(false)
    panel.btnClose:SetVisible(false)
    panel.btnMaxim:SetVisible(false)
    panel.btnMinim:SetVisible(false)
    panel:SetDraggable(false)
	panel.Paint = function (self,w,h)
			surface.SetDrawColor(Rashkinsk.UIColors.Grey)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(Rashkinsk.UIColors.Red)
			--surface.SetMaterial(Rashkinsk.UIMaterials.grad)
			surface.DrawRect(0, 0, w, scrh1*35)
			--surface.DrawTexturedRect(0, 0, w, 30)
			--surface.DrawTexturedRect(0, 0, w, 30)
			--surface.DrawTexturedRect(0, 0, w, 30)

			draw.SimpleText(title,"RashkinskRodchenkoVote",scrw1*10,scrh1*9,color_white)
	end
    function panel:Close()
        --PanelNum = PanelNum - scrw1*questionw
		PanelNum = 0
        QuestionVGUI[quesid .. "vote"] = nil

        local num = 0
        for k,v in SortedPairs(VoteVGUI) do
            v:SetPos(num, ScrH() / 2 - 50)
            num = num + scrw1*questionw
        end

        for k,v in SortedPairs(QuestionVGUI) do
            v:SetPos(num, ScrH() / 2 - 50)
            num = num + scrw1*questionw
        end
        self:Remove()
    end

    function panel:Think()
        title = "Вопрос: "..math.Clamp(math.ceil(timeleft - (CurTime() - OldTime)), 0, 9999)
        if timeleft - (CurTime() - OldTime) <= 0 then
            panel:Close()
        end
    end

    panel:SetKeyboardInputEnabled(false)
    panel:SetMouseInputEnabled(true)
    panel:SetVisible(true)

	for i = 41, string.len(question), 41 do
        if not string.find(string.sub(question, i - 39, i), "\n", 1, true) then
            question = string.sub(question, 1, i) .. "\n" .. string.sub(question, i + 1, string.len(question))
        end
    end

    local label = vgui.Create("DLabel")
    label:SetParent(panel)
    label:SetPos(scrw1*10, scrh1*10)
    label:SetText(DarkRP.deLocalise(question))
	label:SetSize(scrw1*(questionw-50), scrh1*(questionh-20))
    --label:SizeToContents()
	label:SetFont("RashkinskFuturaSmall")
    label:SetVisible(true)

    --[[ local nextHeight = label:GetTall() > 78 and label:GetTall() - 78 or 0 -- Make panel taller for divider and buttons
    panel:SetTall(panel:GetTall() + nextHeight) ]]


    local ybutton = vgui.Create("DImageButton")
    ybutton:SetParent(panel)
    ybutton:SetPos(scrw1*questionw-scrw1*16*2-scrw1*10*2, panel:GetTall() - scrh1*35)
    ybutton:SetSize( 16*scrw1, 16*scrh1 )			// OPTIONAL: Use instead of SizeToContents() if you know/want to fix the size
	ybutton:SetImage( Rashkinsk.UIMaterials.check )
    ybutton:SetCommand("!")
    ybutton:SetVisible(true)
    ybutton.DoClick = function()
        LocalPlayer():ConCommand("ans " .. quesid .. " 1\n")
        panel:Close()
    end

    local nbutton = vgui.Create("DImageButton")
    nbutton:SetParent(panel)
    nbutton:SetPos(scrw1*questionw-scrw1*16-scrw1*10, panel:GetTall() - scrh1*35)
    nbutton:SetSize( 16*scrw1, 16*scrh1 )			// OPTIONAL: Use instead of SizeToContents() if you know/want to fix the size
	nbutton:SetImage( Rashkinsk.UIMaterials.cross )
    nbutton:SetCommand("!")
    nbutton:SetVisible(true)
    nbutton.DoClick = function()
        LocalPlayer():ConCommand("ans " .. quesid .. " 2\n")
        panel:Close()
    end

    --PanelNum = PanelNum + scrw1*questionw + scrw1*3
	PanelNum = 0
    QuestionVGUI[quesid .. "vote"] = panel
end
usermessage.Hook("DoQuestion", MsgDoQuestion)

local function KillQuestionVGUI(msg)
    local id = msg:ReadString()

    if QuestionVGUI[id .. "ques"] and QuestionVGUI[id .. "ques"]:IsValid() then
        QuestionVGUI[id .. "ques"]:Close()
    end
end
usermessage.Hook("KillQuestionVGUI", KillQuestionVGUI)

local function DoVoteAnswerQuestion(ply, cmd, args)
    if not args[1] then return end

    local vote = 0
    if tonumber(args[1]) == 1 or string.lower(args[1]) == "yes" or string.lower(args[1]) == "true" then vote = 1 end

    for k,v in pairs(VoteVGUI) do
        if IsValid(v) then
            local ID = string.sub(k, 1, -5)
            VoteVGUI[k]:Close()
            RunConsoleCommand("vote", ID, vote)
            return
        end
    end

    for k,v in pairs(QuestionVGUI) do
        if IsValid(v) then
            local ID = string.sub(k, 1, -5)
            QuestionVGUI[k]:Close()
            RunConsoleCommand("ans", ID, vote)
            return
        end
    end
end
concommand.Add("rp_vote", DoVoteAnswerQuestion)



