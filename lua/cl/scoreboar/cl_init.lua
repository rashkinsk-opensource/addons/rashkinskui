local mainsb = {ptbl ={}}

local btn_h = ScrH()*.04

local screenwidth = ScrW()
local screenheight = ScrH()

local scrw1 = 1/1350*screenwidth
local scrh1 = 1/768*screenheight

local playerButtons = { }

timer.Simple( .1, function()
	function GAMEMODE:ScoreboardShow()

		local drawRashkinskBGSB = GWEN.CreateTextureBorder(0, 0, 4095, 3300, 0, 0, 0,0, Rashkinsk.UIMaterials.bgscrcherno)
		local drawRashkinskBGSBbts = GWEN.CreateTextureBorder(0, 0, 4096, 118, 0, 0, 0,0, Rashkinsk.UIMaterials.gradsb)
		mainsb.ptbl.sbframe = vgui.Create("DFrame")
		mainsb.ptbl.sbframe:SetTitle("")
		mainsb.ptbl.sbframe:SetSize(ScrW()*.9,ScrH()*.8)
		mainsb.ptbl.sbframe:Center() 
		mainsb.ptbl.sbframe:SetAlpha(0)
		mainsb.ptbl.sbframe:AlphaTo(255,0.2,0)
		mainsb.ptbl.sbframe:MakePopup()
		mainsb.ptbl.sbframe:ShowCloseButton(false)
		mainsb.ptbl.sbframe:SetDraggable(false)

		mainsb.ptbl.sbframe.Paint = function(self,w,h)
			drawRashkinskBGSB(0, 0, w, h)
			--draw.RoundedBox(0,0,143,w,h,Color(0,0,0))
			draw.SimpleText(GetHostName().." "..#player.GetAll() .. "/" .. game.MaxPlayers(),"RashkinskRodchenko3",w/2,ScrH()*.045		,Color(255,255,255),1)
			draw.SimpleText("Имя","RashkinskRodchenko4",w*.17,ScrH()*.129,Color(255,255,255),1)
			draw.SimpleText("Работа","RashkinskRodchenko4",w*.385,ScrH()*.129,Color(255,255,255),1)
			draw.SimpleText("Время","RashkinskRodchenko4",w*.630,ScrH()*.129,Color(255,255,255),1)
			draw.SimpleText("Пинг","RashkinskRodchenko4",w*.860,ScrH()*.129,Color(255,255,255),1)

		end

		mainsb.ptbl.scrol = vgui.Create("DScrollPanel", mainsb.ptbl.sbframe)
		mainsb.ptbl.scrol:SetPos(ScrW()*.033,ScrH()*.2)
		mainsb.ptbl.scrol:SetSize(mainsb.ptbl.sbframe:GetWide()-ScrW()*.028,mainsb.ptbl.sbframe:GetTall()-ScrH()*.2)
		mainsb.ptbl.scrol.VBar:SetHideButtons(true)
		mainsb.ptbl.scrol.VBar.Paint = function (self,w, h)
		end
		mainsb.ptbl.scrol.VBar.btnGrip.Paint = function (self,w, h)
			surface.SetDrawColor(41,41,41)
			surface.DrawRect(2, 0, w - 2, h)
		end

		mainsb.ptbl.btns = {}
		local avatarr
		local plys = player.GetAll()
		table.sort( plys, function( a, b ) return a:Team() < b:Team() end )
		for k,v in pairs(plys) do
			local currentTime = v:GT_GetTime()
			local button = vgui.Create("DButton",mainsb.ptbl.scrol)
			button:SetText("")
			button:Dock(TOP)
			button:SetTall(btn_h)
			button:DockMargin(0, 0, 0, 0)
			button.Paint = function (self,w,h)
				local colorsb
				if math.fmod(k,2)~=0 then
					colorsb = Color(255,255,255)
				else 
					colorsb = Color(239,239,237)
				end	
				if IsValid(v) then
					teamCol = team.GetColor(v:Team())
					teamName = v:getDarkRPVar("job") or ""
					plyname = v:Nick()
					plyping = v:Ping()
				else
					teamCol = Color(255,0,0)
					teamName = "с сервера"
					plyname = "Вышел"
					plyping = "0"
				end
				drawRashkinskBGSBbts(0,btn_h,w,btn_h)
				draw.RoundedBox(0,0,0,w,btn_h+1,colorsb)
				
				--surface.SetDrawColor(0,0,0,255)
				--surface.DrawOutlinedRect(0,0,w,h)
				--surface.DrawOutlinedRect(0, 0, w, 25)
				draw.SimpleText(plyname,"RashkinskFutura4", w*.14,(btn_h)/2,Color(0,0,0),1,1)
				draw.SimpleText(teamName,"RashkinskFutura4", w*.36,(btn_h)/2,Color(0,0,0),1,1)
				draw.SimpleText(GTawards:GetConvertedTime(currentTime, "string") or "0","RashkinskFutura4", w*.61,(btn_h)/2,Color(0,0,0),1,1)
				draw.SimpleText(plyping,"RashkinskFutura4", w*.86,(btn_h)/2,Color(0,0,0),1,1)
			
			end
			button.DoClick = function (self,w,h)
				surface.PlaySound('lampoviy_server/playerui.ogg')
				for k, v in pairs(mainsb.ptbl.btns) do
					if v ~= button and v.Opened then v:SizeTo(ScrW()*.73, ScrH()*.05, 0.25) v.Opened = false end
				end
				button:SizeTo(-1, button.Opened and btn_h or btn_h*2, 0.25)
				button.Opened = not button.Opened		
			end
			local selectedname = "Bot"
			if !v:IsBot() and IsValid(v) then
				selectedname = steamworks.GetPlayerName( v:SteamID64() )
			else
				selectedname = "-"
			end
			if (!LocalPlayer():IsUserGroup("donate_blat") or !LocalPlayer():IsUserGroup("donate_blat_plus") or !LocalPlayer():IsUserGroup("moder_blat") or !LocalPlayer():IsUserGroup("moder_blat_plus") or !LocalPlayer():IsUserGroup("sponsor") or !LocalPlayer():IsUserGroup("moderator") or !LocalPlayer():IsUserGroup("admin") or !LocalPlayer():IsUserGroup("superadmin") or !LocalPlayer():IsUserGroup("manager") or !LocalPlayer():IsUserGroup("developer") or !LocalPlayer():IsUserGroup("creator")) then
			playerButtons = {
				{
					text = selectedname,
					func = function(ply)
						ply:ShowProfile()
					end
				},
				{
					text = 'SteamID',
					func = function(ply)
						SetClipboardText(ply:SteamID())
						chat.AddText(Color(64, 228, 64), 'SteamID скопирован в буфер обмена!')
					end
				},
				{
					text = 'goto',
					func = function(ply)
						RunConsoleCommand("ulx", "goto", ply:Name())
					end
				},
				{
					text = 'bring',
					func = function(ply)
						RunConsoleCommand("ulx", "bring", ply:Name())
					end
				},
				{
					text = 'return',
					func = function(ply)
						RunConsoleCommand("ulx", "return", ply:Name())
					end
				},
				{
					text = 'spectate',
					func = function(ply)
						RunConsoleCommand("ulx", "spectate", ply:Name())
					end
				},
			}
			else
			playerButtons = {
				{
					text = steamworks.GetPlayerName( v:SteamID64()),
					func = function(ply)
						ply:ShowProfile()
					end
				},
				{
					text = 'SteamID',
					func = function(ply)
						SetClipboardText(ply:SteamID())
						chat.AddText(Color(64, 228, 64), 'SteamID скопирован в буфер обмена!')
					end
				},
			}
			end
				for i, tbl in pairs(playerButtons) do
					local b = vgui.Create('DButton', button)
					b:SetSize(ScrW()*.4*0.365, btn_h*0.75)
					b:SetPos((i-1) * (b:GetWide()*1), btn_h*1.2)
					b.Paint = function(self, w, h)
					--	draw.RoundedBox(0,0,0,w,h,Color(0,0,0))
					--	draw.SimpleText(tbl.text, 'RashkinskFutura4', w/2-1, h/2+1,Color(0,0,0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						draw.SimpleText(tbl.text, 'RashkinskFutura4', w/2, h/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						return true
					end
					b.DoClick = function()
						tbl.func(v)
					end
				end

			if v:IsAdmin()  then
				local imagead = vgui.Create("DImage", button)
				imagead:SetSize( btn_h - 8, btn_h - 8 )
				imagead:SetPos( mainsb.ptbl.scrol:GetWide()*.96, 4 )
				imagead:SetImage(Rashkinsk.UIMaterials.crown)
			end

			local avatar = vgui.Create("AvatarImage", button)
			avatar:SetSize( btn_h - 4, btn_h - 4 )
			avatar:SetPos( 2, 2 )
			avatar:SetPlayer( v, btn_h - 4 )
		end
	end

	function GAMEMODE:ScoreboardHide()
		if IsValid(mainsb.ptbl.sbframe) then
			mainsb.ptbl.sbframe:AlphaTo(0,0.2,0)
			timer.Simple(0.2, function ()
				mainsb.ptbl.sbframe:Remove()
				mainsb.ptbl.sbframe = nil
			end)
		end
		return true
	end	
end)