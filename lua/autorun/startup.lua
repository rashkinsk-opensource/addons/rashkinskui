AddCSLuaFile()

if SERVER then
		resource.AddFile("materials/rashkinskui/grad.png")
		resource.AddFile("materials/rashkinskui/bgscrcherno.png")
		resource.AddFile("materials/rashkinskui/bgcherno.png")
		resource.AddFile("materials/rashkinskui/bghud.png")
		resource.AddFile("materials/rashkinskui/icons/check.png")
		resource.AddFile("materials/rashkinskui/icons/cross.png")
		resource.AddFile("materials/rashkinskui/icons/food.png")
		resource.AddFile("materials/rashkinskui/icons/health.png")
		resource.AddFile("materials/rashkinskui/icons/armor.png")
		resource.AddFile("materials/rashkinskui/icons/wanted.png")
		resource.AddFile("materials/rashkinskui/icons/weaponwarrant.png")
		resource.AddFile("materials/rashkinskui/icons/commands.png")
		resource.AddFile("materials/rashkinskui/icons/jobs.png")
		resource.AddFile("materials/rashkinskui/icons/money.png")
		resource.AddFile("materials/rashkinskui/icons/speaker.png")
		resource.AddFile("materials/rashkinskui/icons/crown.png")
		resource.AddFile("materials/rashkinskui/gradsb.png")
		resource.AddSingleFile("resource/fonts/futura-normal.ttf")
		resource.AddSingleFile("resource/fonts/rodchenkocct.ttf")
end

Rashkinsk = Rashkinsk or {}

Rashkinsk.UIMaterials = {
	["bgscrcherno"] = Material("materials/rashkinskui/bgscrrashkinsk.png", "noclamp"),
	["bgcherno"] = Material("materials/rashkinskui/bgrashkinsk.png", "noclamp"),
	["bghud"] = Material("materials/rashkinskui/bghud_rashk.png", "noclamp"),
	["grad"] = Material("materials/rashkinskui/grad.png", "noclamp"),
	["check"] = "materials/rashkinskui/icons/check.png",
	["cross"] = "materials/rashkinskui/icons/cross.png",
	["food"] = Material("materials/rashkinskui/icons/food.png", "noclamp"),
	["health"] = Material("materials/rashkinskui/icons/health.png", "noclamp"),
	["armor"] = Material("materials/rashkinskui/icons/armor.png", "noclamp"),
	["wanted"] = Material("materials/rashkinskui/icons/wanted.png", "noclamp"),
	["weaponwarrant"] = Material("materials/rashkinskui/icons/weaponwarrant.png", "noclamp"),
	["commands"] = Material("materials/rashkinskui/icons/commands.png", "noclamp"),
	["jobs"] = Material("materials/rashkinskui/icons/jobs.png", "noclamp"),
	["money"] = Material("materials/rashkinskui/icons/money.png", "noclamp"),
	["speaker"] = Material("materials/rashkinskui/icons/speaker.png", "noclamp"),
	["gradsb"] = Material("materials/rashkinskui/gradsb.png"),
	["crown"] = "materials/rashkinskui/icons/crown.png",
	["admin"] = "materials/rashkinskui/icons/commands.png",
}
Rashkinsk.UIColors = {
	["Red"] = Color(127, 15, 15),
	["BrightRed"] = Color(200, 0, 0),
	["Grey"] = Color(50, 50, 50),
	["LightGrey"] = Color(100, 100, 100),
	["Green"] = Color(0, 200, 0),
	["Blue"] = Color(50, 50, 150),
	["Bluef"] = Color(54, 116, 182),
	["cht"] = Color(29, 29, 27)
}

if CLIENT then
	function Rashkinsk.DrawMasked(maskFunction, drawFunction)
		render.ClearStencil()
		render.SetStencilEnable(true)
	 
		render.SetStencilWriteMask(1)
		render.SetStencilTestMask(1)
	 
		render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
		render.SetStencilPassOperation( STENCILOPERATION_ZERO )
		render.SetStencilZFailOperation( STENCILOPERATION_ZERO )
		render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
		render.SetStencilReferenceValue( 1 )
	 
		maskFunction()
	 
		render.SetStencilFailOperation(STENCILOPERATION_ZERO)
		render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
		render.SetStencilZFailOperation(STENCILOPERATION_ZERO)
		render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_EQUAL)
		render.SetStencilReferenceValue(1)
	 
		drawFunction()
	 
		render.SetStencilEnable(false)
		render.ClearStencil()
	end
	
	//Fix draw.RoundedBox() to work with stencils.
	local mat1 = CreateMaterial( "corner8", "UnlitGeneric", {
		[ "$basetexture" ] = "gui/corner8",	
		[ "$alphatest" ] = 1,
		[ "$translucent" ] = 1,
		[ "$ignorez" ] = 1,
		[ "$vertexcolor" ] = 1,
		[ "$vertexalpha" ] = 1,
		[ "$nolod" ] = 1
	} )
	local mat2 = CreateMaterial( "corner16", "UnlitGeneric", {
		[ "$basetexture" ] = "gui/corner16",	
		[ "$alphatest" ] = 1,
		[ "$translucent" ] = 1,
		[ "$ignorez" ] = 1,
		[ "$vertexcolor" ] = 1,
		[ "$vertexalpha" ] = 1,
		[ "$nolod" ] = 1
	} )
	local mat3 = CreateMaterial( "corner32", "UnlitGeneric", {
		[ "$basetexture" ] = "gui/corner32",	
		[ "$alphatest" ] = 1,
		[ "$translucent" ] = 1,
		[ "$ignorez" ] = 1,
		[ "$vertexcolor" ] = 1,
		[ "$vertexalpha" ] = 1,
		[ "$nolod" ] = 1
	} )
	local mat4 = CreateMaterial( "corner64", "UnlitGeneric", {
		[ "$basetexture" ] = "gui/corner64",	
		[ "$alphatest" ] = 1,
		[ "$translucent" ] = 1,
		[ "$ignorez" ] = 1,
		[ "$vertexcolor" ] = 1,
		[ "$vertexalpha" ] = 1,
		[ "$nolod" ] = 1
	} )
	local mat5 = CreateMaterial( "corner512", "UnlitGeneric", {
		[ "$basetexture" ] = "gui/corner512",	
		[ "$alphatest" ] = 1,
		[ "$translucent" ] = 1,
		[ "$ignorez" ] = 1,
		[ "$vertexcolor" ] = 1,
		[ "$vertexalpha" ] = 1,
		[ "$nolod" ] = 1
	} )
	function Rashkinsk.RoundedBoxEx( bordersize, x, y, w, h, color, tl, tr, bl, br )

		surface.SetDrawColor( color.r, color.g, color.b, color.a )

		-- Do not waste performance if they don't want rounded corners
		if ( bordersize <= 0 ) then
			surface.DrawRect( x, y, w, h )
			return
		end

		x = math.Round( x )
		y = math.Round( y )
		w = math.Round( w )
		h = math.Round( h )
		bordersize = math.min( math.Round( bordersize ), math.floor( w / 2 ) )

		-- Draw as much of the rect as we can without textures
		surface.DrawRect( x + bordersize, y, w - bordersize * 2, h )
		surface.DrawRect( x, y + bordersize, bordersize, h - bordersize * 2 )
		surface.DrawRect( x + w - bordersize, y + bordersize, bordersize, h - bordersize * 2 )

		local tex = mat1
		if ( bordersize > 8 ) then tex = mat2 end
		if ( bordersize > 16 ) then tex = mat3 end
		if ( bordersize > 32 ) then tex = mat4 end
		if ( bordersize > 64 ) then tex = mat5 end

		surface.SetMaterial( tex )

		if ( tl ) then
			surface.DrawTexturedRectUV( x, y, bordersize, bordersize, 0, 0, 1, 1 )
		else
			surface.DrawRect( x, y, bordersize, bordersize )
		end

		if ( tr ) then
			surface.DrawTexturedRectUV( x + w - bordersize, y, bordersize, bordersize, 1, 0, 0, 1 )
		else
			surface.DrawRect( x + w - bordersize, y, bordersize, bordersize )
		end

		if ( bl ) then
			surface.DrawTexturedRectUV( x, y + h -bordersize, bordersize, bordersize, 0, 1, 1, 0 )
		else
			surface.DrawRect( x, y + h - bordersize, bordersize, bordersize )
		end

		if ( br ) then
			surface.DrawTexturedRectUV( x + w - bordersize, y + h - bordersize, bordersize, bordersize, 1, 1, 0, 0 )
		else
			surface.DrawRect( x + w - bordersize, y + h - bordersize, bordersize, bordersize )
		end

	end

	
	
end

Rashkinsk.UIModules = {
	"hud",
	"scoreboar",
	"f4"
}

local function RashkinskLoadModulesCL()
	AddCSLuaFile("cl/cl_font.lua")
	AddCSLuaFile("cl/hud/cl_voting.lua")
	if CLIENT then
		include("cl/cl_font.lua")
		include("cl/hud/cl_voting.lua")
	end
	for _, moduleName in pairs(Rashkinsk.UIModules) do
		AddCSLuaFile("cl/".. moduleName .. "/cl_init.lua")
		if CLIENT then
			include("cl/".. moduleName .. "/cl_init.lua")
		end		
	end
end

local function RashkinskLoadModulesSV()
	local files = file.Find( 'sv/*.lua', 'LUA' )
	table.foreach( files, function( key, gui )
		include( 'sv/' .. gui )
	end )
end

RashkinskLoadModulesCL()
RashkinskLoadModulesSV()

print('---RashkinskUI Load---')